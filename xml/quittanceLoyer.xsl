<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<form class="QuittanceLoyer center" id="formquittance">
   <div class="container">
       <xsl:for-each select="form/fieldset">

      <div class="block{position()}"> 
                <xsl:for-each select="element">
                <xsl:variable name="elementid" select='elementid'/>
                <xsl:variable name="elementclass" select='elementclass'/>
                <xsl:variable name="elementtype" select='elementtype'/>
                <xsl:variable name="elementval" select='elementval'/>
                <input class="{$elementclass}" id="{$elementid}" type="{$elementtype}" value="{$elementval}"/>
                </xsl:for-each>  
      </div>
        </xsl:for-each>
</div>
</form>     
</xsl:template>
</xsl:stylesheet>