<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<form id="formpalet">
    <table>
       <xsl:for-each select="form/fieldset">
            <fieldset id="field_{position()}">
             <legend><xsl:value-of select="fieldsetname" /></legend>
             
                <xsl:for-each select="element">
                    <xsl:variable name="elementid" select='elementid'/>
                    <xsl:variable name="elementclass" select='elementclass'/>
                    <xsl:variable name="elementtype" select='elementtype'/>
                    <xsl:variable name="elementval" select='elementval'/>
                    <xsl:variable name="elementname" select='elementname'/>
                    <label><xsl:value-of select="label"/>:</label>
                    <input class="{$elementclass}" id="{$elementid}" name="{$elementname}" type="{$elementtype}" value="{$elementval}"/>
                </xsl:for-each>
            </fieldset>
        </xsl:for-each>

   </table>
</form>     
</xsl:template>
</xsl:stylesheet>